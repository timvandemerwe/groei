<?php
require "inc/config.php";

// haal alle type opleidingen en landschappen op
$groups = $db->prepare("SELECT * FROM groups");

// plaats de type opleidingen in een array
if ($groups->execute()) {
    $_ALLGROUPS = $groups->fetchAll(PDO::FETCH_OBJ);
}

// wahten tot het opleiding formulier is verzonden
if(isset($_POST['submit_user'])){
    if(isset($_POST['email']) && !empty($_POST['email'] && $_POST['password']) && !empty($_POST['password'])){

        $emailQuery = $db->prepare("SELECT email FROM users");
        if ($emailQuery->execute()) {
            $emails = $emailQuery->fetchAll(PDO::FETCH_OBJ);
        }

        $isValid = true;
        foreach($emails as $singleEmail){
            if($singleEmail->email === $_POST['email']){
                $isValid = false;
            }
        }

        if($isValid){
            // Voorbereiden insert querys opleiding en content
            $user = $db->prepare("INSERT INTO `users` (`email`, `password`, `group_id`) VALUES (:email, :password, :group_id)");

            // Voeg de overige parameters toe aan de opleiding en verzend
            $user->bindValue(":email", $_POST['email'], PDO::PARAM_STR);
            $user->bindValue(":password", $_POST['password'], PDO::PARAM_STR);
            $user->bindValue(":group_id", $_POST['group'], PDO::PARAM_INT);
            $user->execute();

            addMessage("Succesvol aangemaakt", 'De gebbruiker <b>'.$_POST['email'].'</b> is succesvol aangemaakt.', 'success');
            header("Location: allUsers.php");
            exit;
        } else {
            $_VIEWDATA["naam"] = "Email bestaat al.";
        }
    } else {
        $_VIEWDATA["naam"] = "Vul alle velden in.";
    }
}






if(isset($_POST['submit_group'])){
    // Voorbereiden insert querys opleiding en content
    $addGroup = $db->prepare("INSERT INTO `groups` (`name`, `description`) VALUES (:name, :description)");

    // Voeg de overige parameters toe aan de opleiding en verzend
    $addGroup->bindValue(":name", $_POST['group_name'], PDO::PARAM_STR);
    $addGroup->bindValue(":description", $_POST['group_description'], PDO::PARAM_STR);
    $addGroup->execute();

	addMessage("Succesvol aangemaakt", 'De groep <b>'.$_POST['group_name'].'</b> is succesvol aangemaakt.', 'success');
	header("Location: allUsers.php");
    exit;

}






$_VIEW = "views/userAdd.php";

require_once "templates/default.php";