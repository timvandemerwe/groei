<?php
require "inc/config.php";

header('Content-Type: image/png');
header('Content-Disposition: attachment; filename="'.trim(($_GET["filename"] ?? 'qr-code')).'.png"');
//exit(__DIR__.'/generateQRCode.php?content='.$_GET["content"] ?? "");

$img = imagecreatetruecolor(2480  , 2480);

$white = imagecolorallocate($img, 255, 255, 255);

$black = imagecolorallocate($img, 0, 0, 0);

imagefill($img, 0, 0, $white);

$qr = @imagecreatefrompng(DOMAIN.'/generateQRCode.php?content='.urlencode($_GET["content"] ?? ""));
//exit(__FILE__.'/generateQRCode.php?content='.$_GET["content"] ?? "");

$height = imagesy($img);
$width = imagesx($img);
$qrWH = imagesx($qr);
$qrWHNew = 2480;
$fontSize = 60;

imagecopyresized($img, $qr, ($width / 2 - ($qrWHNew / 2)), $height - $qrWHNew, 0, 0, $qrWHNew, $qrWHNew, $qrWH, $qrWH);

// Set the enviroment variable for GD
putenv('GDFONTPATH=' . realpath('.'));
imagettftext($img, $fontSize, 0, 20, 20 + $fontSize, $black, /*Font file*/ "Arial", "Info over: \n\n".($_GET["name"] ?? ''));

//imagestring($img, 10, , 10, ($_GET["name"] ?? ''), 1);
imagepng($img);
imagedestroy($img);

