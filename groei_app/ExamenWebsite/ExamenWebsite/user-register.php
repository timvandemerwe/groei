<?php
require "inc/config.php";

require_once 'inc/class/User.class.php';


/*
 * Kijken of het formulier is verzonden.
 */
if (isset($_POST["submit"])) {
    $email = trim($_POST["email"] ?? "");
    $wachtwoord = trim($_POST["wachtwoord"] ?? "");
    $voornaam = trim($_POST["voornaam"] ?? "");
    $achternaam = trim($_POST["achternaam"] ?? "");

    $user = new User($db, $email, $wachtwoord);

    // Kijken of het een geldig mail adres is.
    if(empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $_VIEWDATA["email"] = "vul een geldig email in.";
    }

    //Kijken of het een geldig wachtwoord is.
    if(empty($wachtwoord)) {
        $_VIEWDATA["wachtwoord"] = "vul een wachtwoord in.";
    } else if (strlen($wachtwoord) < 8) {
        $_VIEWDATA["wachtwoord"] = "Het wachtwoord moet minimaal 8 karakters bevatten.";
    }

    // Kijken of het wachtwoord 1 cijfer bevat.
    else if (!preg_match("#[0-9]+#", $wachtwoord)) {
         $_VIEWDATA["wachtwoord"] = "Het wachtwoord moet minimaal 1 cijfer bevatten.";
    }

    // Kijken of het wachtwoord 1 hoofdletter bevat.
    else if (!preg_match("#[A-Z]+#", $wachtwoord)) {
        $_VIEWDATA["wachtwoord"] = "Het wachtwoord moet minimaal 1 hoofdletter bevatten.";
    }

    // Kijken of de voornaam is ingevuld.
    if(empty($voornaam)) {
        $_VIEWDATA["voornaam"] = "vul een voornaam in.";
    }

    // Kijken of de achternaam is ingevuld.
    if(empty($achternaam)) {
        $_VIEWDATA["achternaam"] = "vul een achternaam in.";
    }

    if (empty($_VIEWDATA)) {


    $user->voornaam = $voornaam;
    $user->achternaam = $achternaam;

    /*
     * Kijken of dit email adres al geregistreerd is zo niet maak een nieuwe aan.
     */
    if ($user->exists()) {
        $_VIEWDATA["email"] = "Er bestaat al een gebruiker met dit e-mail adres.";
    } else {
        if ($user->register()) {
            addMessage("Succesvol aangemaakt", 'De gebruiker <b>'.$user->voornaam.'</b> is succesvol aangemaakt.', 'success');
			header("Location: users.php");
			exit;
        } else {
            $_VIEWDATA["register"] = "Gebruiker kon niet aangemaakt worden";
        }
    }
}

}

$_VIEW = "views/register.php";

require_once "templates/default.php";