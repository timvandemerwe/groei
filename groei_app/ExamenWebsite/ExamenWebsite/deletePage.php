<?php

require "inc/config.php";

// check of user is admin moet nog
echo '<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Weet je zeker dat je deze opleiding wilt verwijderen?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="modal-btn-si">Ja</button>
        <button type="button" class="btn btn-primary" id="modal-btn-no">Nee</button>
      </div>
    </div>
  </div>
</div>

<div class="alert" role="alert" id="result"></div>';

// opleiding
if($_GET["s"] == "opleiding") {
    $query = $db->prepare("DELETE FROM `opleiding` WHERE id = :id");
    $query->bindValue(":id", $_GET['id'], PDO::PARAM_INT);

    if ($query->execute()) {
        header("Location: adminAllPages.php");
        exit();
        // melding of zeker weten moet nog
        //$confirm = true;
    }
} elseif($_GET["s"] == "landschap") {
    $query = $db->prepare("DELETE FROM `landschap` WHERE id = :id");
    $query2 = $db->prepare("DELETE FROM `opleiding` WHERE landschap_id = :id");
    $query->bindValue(":id", $_GET['id'], PDO::PARAM_INT);
    $query2->bindValue(":id", $_GET['id'], PDO::PARAM_INT);

    if ($query->execute() && $query2->execute()) {

        header("Location: adminAllPages.php");
        exit();
        // melding of zeker weten moet nog
        //$confirm = true;
    }
}

$_VIEW = "views/adminAllPages.php";
require_once "templates/default.php";