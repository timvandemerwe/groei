<?php
require "inc/config.php";

checkLogin();

$id = $_GET["id"] ?? "";

// De gebruiker verwijderen uit de database
$query = $db->prepare("
    SELECT voornaam FROM user WHERE id = :id;
    DELETE FROM user WHERE id = :id;
");

$query->bindValue(":id", $id, PDO::PARAM_INT);
//var_dump($query); // kijken wat er in staat, als er staat hoeveel gebruikers er zijn, dit gebruiken met .count() ofzo en dan if >1
    if($id != $_SESSION["UID"]) {
        if ($query -> execute()) {
	        if ($user = $query -> fetch(PDO::FETCH_OBJ)) {

		        addMessage("Succesvol verwijderd", 'De gebruiker <b>'.$user->voornaam.'</b> is succesvol verwijderd.', 'success');
	        }

        }
    } else {
        addMessage("Niet toegestaan", 'U kunt u zelf niet verwijderen.', 'error');
    }
    header("Location: users.php");
    exit;