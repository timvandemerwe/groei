<div class="container">
<br /><br /><br /><br />
    <!-- Een knop naar het toevoegen van nieuwe paginas -->
    <a class="btn btn-primary nieuwLandschapKnop" href="userAdd.php" role="button">Gebruiker / Groep Toevoegen</a>
    <!-- De tabel waarin alle landschappen met bijbehorende opleidingen worden weergegeven -->
    <table class="table table-striped" style="width:100%">
        <!-- Defineer de tabel titels -->
        <tr class="titelcss">
            <th class="w-75">Naam</th>
            <th class="w-25">Acties</th>
        </tr>

        <!-- Loop voor elk landschap 1x door het landschap heen en door alle bijbehorende opleidingen -->
        <!-- Display vervolgens de naam van de opleiding/landschap en de actie knoppen -->
        <?php
        foreach($allGroups as $singleGroup){
        echo '<tr class="titelcss">
            <td class="namebold">' . $singleGroup['name'] . '</td>
            <td>
				<a data-toggle="tooltip" data-placement="top" title="Bewerken" class="btn btn-outline-primary" href="editGroup.php?id='.$singleGroup['id'].'"><i class="fas fa-edit"></i></a>
				<a data-toggle="tooltip" data-confirm-box="delete" data-message="Weet u zeker dat u het landschap <b>'.$singleGroup['name'].'</b> met onderstaande opleidingen wilt verwijderen?" data-action-url="deletePage.php?id='.$singleGroup['id'].'&s=landschap" data-placement="top" title="Verwijderen" class="btn btn-outline-danger" href=""><i class="fas fa-trash"></i></a>
			</td>
          </tr>';


        foreach($singleGroup['users'] as $opleiding){
         echo '<tr class="highlight titelcss">
            <td>>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;' . $opleiding['name'] . '</td>
            <td>
				<a data-toggle="tooltip" data-placement="top" title="Bewerken" class="btn btn-outline-primary" href="editGroeiUser.php?id='.$opleiding['id'].'"><i class="fas fa-edit"></i></a>
				<a data-toggle="tooltip" data-confirm-box="delete" data-message="Weet u zeker dat u de opleiding <b>'.$opleiding['name'].'</b> wilt verwijderen?" data-action-url="deletePage.php?id='.$opleiding['id'].'&s=opleiding" data-placement="top" title="Verwijderen" class="btn btn-outline-danger" href=""><i class="fas fa-trash"></i></a>
            </td>
          </tr>';
    }

    echo '<tr class="highlight">
            <td><br /></td>
            <td></td>

          </tr>';
    }

    /* test*/
        ?>
    </table>
</div>