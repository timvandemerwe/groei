<div class="container">
<br /><br /><br /><br />
    <!-- knop voor nieuwe gebruiker-->
    <a class="btn btn-primary nieuwLandschapKnop" href="user-register.php" role="button">Nieuwe Gebruiker</a>
    <!--laat alle gebruikers zien-->
    <table class="table table-striped" style="width:100%">
        <tr class="titelcss">
            <th>#</th>
            <th>Voornaam</th>
            <th>Achternaam</th>
            <th>Email</th>
            <th class="w-25">Acties</th>
        </tr>
        <?php
        foreach($_USERS as $user){
            echo '<tr class="titelcss">
                <th scope="row">' . $user->id. '</th>
                <td>' . $user->voornaam. '</td>
                <td>' . $user->achternaam. '</td>
                <td>' . $user->email. '</td>
                <td>
				    <a data-toggle="tooltip" data-placement="top" title="Bewerken" class="btn btn-outline-primary" href="user-edit.php?id='.$user->id.'"><i class="fas fa-edit"></i></a>
				    <a data-toggle="tooltip" data-confirm-box="delete" data-message="Weet u zeker dat u de gebruiker <b>'.$user->voornaam.'</b> wilt verwijderen?" data-action-url="user-delete.php?id='.$user->id.'" data-placement="top" title="Verwijderen" class="btn btn-outline-danger" href=""><i class="fas fa-trash"></i></a>
			    </td>
              </tr>';

        }



    /* test*/
        ?>
    </table>
</div>