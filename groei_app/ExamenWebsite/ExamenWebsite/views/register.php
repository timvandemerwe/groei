<div class="container">
    <div id="registerform">
        <h1 id="registertitel">
            Registreren
        </h1>
        <!--registreer form-->
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
            <label class="registerlabel" for="email">Email adres</label>
            <br />
            <input class="registerinput" name="email" value="<?php echo $_POST["email"] ?? ""; ?>" type="text" placeholder="Email" />
            <?php echo getError("email"); ?>
            <br />

            <label class="registerlabel" for="wachtwoord">Wachtwoord</label>
            <br />
            <input class="registerinput" name="wachtwoord" type="password" placeholder="Wachtwoord" />
            <?php echo getError("wachtwoord"); ?>
            <br />

            <label class="registerlabel" for="voornaam">Voornaam</label>
            <input class="registerinput" name="voornaam" value="<?php echo $_POST["voornaam"] ?? ""; ?>" type="text" placeholder="Voornaam" />
            <?php echo getError("voornaam"); ?>
            <br />

            <label class="registerlabel" for="achternaam">Achternaam</label>
            <br />
            <input class="registerinput" name="achternaam" value="<?php echo $_POST["achternaam"] ?? ""; ?>" type="text" placeholder="Achternaam" />
            <?php echo getError("achternaam"); ?>
            <br />
            <!--knop om je registreer gegevens te submitten-->
            <input id="
                registersubmit" name="submit" type="submit" value="Registreer" />
        </form>
        
    </div>
</div>