<?php
require "inc/config.php";

$_VIEW = "views/login.php";

require_once 'inc/class/User.class.php';

if (isset($_POST["submit"])) {
    $email = $_POST["email"] ?? "";
    $password = $_POST["password"] ?? "";

    // als allebei de gegevens ingevuld zijn
    if(!empty($email) && !empty($password)){
        // maak een nieuwe gebruiker aan
        $user = new User($db, $email, $password);
        // als de gebruiker inlogd, verwijs door naar de de functie en dan verwijzen naar de homepagina
        if ($user->login()) {
            $_SESSION["UID"] = $user->id;

            header("Location: allUsers.php");

            exit;
        } else {
            $_VIEWDATA["login"] = "Gebruikersnaam en/of wachtwoord is verkeerd";
        }
    } else {
        $_VIEWDATA["login"] = "Vul iets in bij gebruikersnaam en wachtwoord";
    }
}

require_once "templates/default.php";