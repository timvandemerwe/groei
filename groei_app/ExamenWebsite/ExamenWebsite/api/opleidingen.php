<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Content-Type: application/json');

require "../inc/config.php";

$id = $_GET["landschap_id"] ?? "";



$query = $db->prepare("
		SELECT landschap.naam, content.content FROM landschap
		INNER JOIN content ON landschap.content_id = content.id
		WHERE landschap.id = :landschap_id;
        SELECT opleiding.id, opleiding.naam, opleiding.duur, opleiding.niveau, type.naam type, content.content, afbeelding.url img, landschap.kleur FROM opleiding
        INNER JOIN content ON opleiding.content_id = content.id
		INNER JOIN type ON opleiding.type_id = type.id
        INNER JOIN landschap ON opleiding.landschap_id = landschap.id
        LEFT JOIN afbeelding ON opleiding.afbeelding_id = afbeelding.id
        WHERE opleiding.landschap_id = :landschap_id;
    ");

$query->bindValue(":landschap_id", $id, PDO::PARAM_INT);

$result = array();

/*
 * Query uitvoeren en het resultaat terug sturen als JSON
 */
if ($query->execute()) {
    $result["landschap"] = $query->fetch(PDO::FETCH_OBJ);
    if ($query->nextRowset()) {
        $result["opleidingen"] = $query->fetchAll(PDO::FETCH_OBJ);
    }

    echo json_encode($result);
} else echo "[]";


?>