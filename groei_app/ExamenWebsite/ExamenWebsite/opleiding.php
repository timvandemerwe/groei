<?php
require "inc/config.php";

$study = $db->prepare("SELECT * FROM opleiding WHERE id=" . $_GET['id']);

$currentIndex = 0;
// lege array opzetten waar alle opleidingen in worden opgeslagen
$allStudies = array();

// haal alle opleidingen op met een bepaald id en zet het object in een var
if ($study->execute()) {
    $_ALLSTUDIES = $study->fetchAll(PDO::FETCH_OBJ);
}

// loop door de $_ALLSTUDIES heen
foreach($_ALLSTUDIES as $singleStudy){
    // haal de afbeelding van de opleiding op doormiddel van het id die is opgeslagen in de opldieng db
    $image = $db->prepare("SELECT url FROM afbeelding WHERE id = :id");
    $image->bindValue(":id", $singleStudy->afbeelding_id, PDO::PARAM_INT);
    if ($image->execute()) {
        $singleImage = $image->fetchAll(PDO::FETCH_OBJ);
    }
    //die($db->errorInfo());

    // voeg de url van de afbeelding toe aan de array
    $allStudies[$currentIndex]['url'] = $singleImage[0]->url;

    $content = $db->prepare("SELECT content FROM content WHERE id=" . $singleStudy->content_id);
    if ($content->execute()) {
        $singleContent = $content->fetchAll(PDO::FETCH_OBJ);
    }

    $type = $db->prepare("SELECT naam FROM type WHERE id = :id");
    $type->bindValue(":id", $singleStudy->type_id, PDO::PARAM_INT);

    if ($type->execute()) {
        $allStudies[$currentIndex]['type'] = $type->fetch(PDO::FETCH_OBJ)->naam;
    }

    $landschap = $db->prepare("SELECT naam FROM landschap WHERE id = :id");
    $landschap->bindValue(":id", $singleStudy->landschap_id, PDO::PARAM_INT);

    if ($landschap->execute()) {
        $allStudies[$currentIndex]['landschap'] = $landschap->fetch(PDO::FETCH_OBJ)->naam;
    }

    $allStudies[$currentIndex]['content'] = $singleContent[0]->content;

    //voeg de overige informatie toe aan de array
    $allStudies[$currentIndex]['name'] = $singleStudy->naam;
    $allStudies[$currentIndex]['time'] = $singleStudy->duur;
    $allStudies[$currentIndex]['niveau'] = $singleStudy->niveau;
    $allStudies[$currentIndex]['landschap_id'] = $singleStudy->landschap_id;


    $currentIndex++;
}

$_VIEW = "views/opleiding.php";

require_once "templates/default.php";