<!--container -->
<div class="container">
    <!-- Error bij lege naam word hier gedisplayd -->
  <?php echo getError("naam");?>
<br />
 <!-- Selecteer of het een opleiding of landschap is -->
<h1 class="heading-text">Nieuwe gebruiker / groep:</h1>
<label for="typeOfPage" class="label-text">Type:</label>
    <select class="custom-select" name="typeOfPage" id="typeOfPage">
        <option selected disabled>Maak uw keuze</option>
        <option value="1">Gebruiker</option>
        <option value="2">Groep</option>
    </select>

    <hr class="line" />

        <!-- Invul formulier voor een opleiding -->
    <form method="post" enctype="multipart/form-data" id="studyForm" class="hide">
          <label class="label-text" for="email">email:</label>
          <input class="custom-input" name="email" id="Email" type="text" />
            <label class="label-text" for="password">wachtwoord:</label>
          <input class="custom-input" name="password" id="password" type="text" /> 
          <div onclick="randomPassword()" class="generate_password">Genereer wachtwoord</div>
          <br /><br />
          <label class="label-text" for="group">Groep:</label>
          <select class="custom-select" name="group" id="group">
            <!-- Plaats alle opleiding types in een select -->
          <?php
          if (isset($_ALLGROUPS)) {
              foreach($_ALLGROUPS as $group){
                  echo '<option value="' . $group->id . '">' . $group->name .'</option>';
              }
          }
          ?> 
              </select>
            <br /><br />
          <input class="custom-submit" name="submit_user" type="submit" value="Voeg gebruiker toe." />
          <br /><br /><br />
    </form>
    <!-- Einde opleiding formulier -->

    <!-- Invul formulier voor landschappen -->

    <form method="post" enctype="multipart/form-data" id="categoryForm" class="hide">
          <label class="label-text" for="group_name">Naam groep:</label>
          <input class="custom-input" name="group_name" id="group_name" type="text" maxlength="150" />
          <label class="label-text" for="group_description">Beschrijving:</label>
          <textarea class="custom-textarea" name="group_description" id="group_description"></textarea>

        <!--Color picker-->
          <!--<input type="text" id="color" name="color" value="#ED135D" />
            <div id="colorpicker"></div> -->
     
          <br />
          <input class="custom-submit" name="submit_group" type="submit" value="Maak groep aan." />
          <br /><br /><br />
    </form>

<!-- Einde landschap formulier -->
</div>
	
	