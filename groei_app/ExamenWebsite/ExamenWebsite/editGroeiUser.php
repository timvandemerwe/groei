<?php
require "inc/config.php";

// ophalen data

$users = $db->prepare("SELECT * FROM users WHERE id=" . $_GET['id']);
$groups = $db->prepare("SELECT * FROM groups");

$currentIndex = 0;
$allUsers = array();

if ($groups->execute()) {
    $_ALLGROUPS = $groups->fetchAll(PDO::FETCH_OBJ);
}

if ($users->execute()) {
    $_ALLUSERS = $users->fetchAll(PDO::FETCH_OBJ);
}


foreach($_ALLUSERS as $singleUser){

    $allUsers[$currentIndex]['email'] = $singleUser->email;
    $allUsers[$currentIndex]['id'] = $singleUser->id;
    $allUsers[$currentIndex]['group_id'] = $singleUser->group_id;
    $allUsers[$currentIndex]['password'] = $singleUser->password;

    $currentIndex++;
}


if(isset($_POST['submit_user'])){
    // Voorbereiden insert querys opleiding en content
    $usersChangeEmail = $db->prepare("UPDATE `users` SET email=:email WHERE id=" . $_GET['id']);
    $usersChangePassword = $db->prepare("UPDATE `users` SET password=:password WHERE id=" . $_GET['id']);
    $usersChangeGroup = $db->prepare("UPDATE `users` SET group_id=:group_id WHERE id=" . $_GET['id']);


    if($allUsers[0]['email'] !== $_POST['email']){
        $usersChangeEmail->bindValue('email', $_POST['email']);
        $usersChangeEmail->execute();
    }

    if($allUsers[0]['group_id'] !== $_POST['group_id']){
        $usersChangeGroup->bindValue('group_id', $_POST['group_id']);
        $usersChangeGroup->execute();
    }

    if($_POST['password'] === $_POST['passwordRepeat']){
        $usersChangePassword->bindValue('password', $_POST['password']);
        $usersChangePassword->execute();
    }


	addMessage("Succesvol aangepast", 'De gebruiker <b>'.$_POST['email'].'</b> is succesvol aangepast.', 'success');
	header("Location: " . $_SERVER['REQUEST_URI']);
	exit;
}



$_VIEW = "views/editGroeiUser.php";

require_once "templates/default.php";