<!--carousel foto-->
<div id="header-img" class="" style="background-image: url('<?php echo ($_CATEGORY->img ? "uploads/".$_CATEGORY->img : "assets/img/landstedeheader.jpg")?>')">
  <?php //echo '<img src="' .  ($_CATEGORY->img ? "uploads/".$_CATEGORY->img : "assets/img/landstedeheader.jpg") . '"  id="" class="" alt="..." />'?>
</div>
<!--tekst-->
<div class="container">
    <div class="tekstdesign">
        <h1>
            <?php echo $_CATEGORY->naam; ?>
        </h1>
        <div>
            <?php echo $_CATEGORY->content; ?>
        </div>
    </div>
    <!--zorgt voor meer ruimte tussen tekst en hr-->
    <br />
    <br />
    <br />
    <hr />

    <!--landschappen kaarten-->
    
    <p>Opleidingen</p>
        <div class="card-deck">
           <?php
           foreach($_ALLSTUDIES as $study){
               echo '<div class="mb-3 col-lg-4"><a style="text-decoration: none;" href="opleiding.php?id='.$study->id.'">
                        <div class="card lg-4 h-100">
                            <img class="card-img-top" src="uploads/'.$study->img.'" alt="Card image cap" />
                            <div class="card-body">
                                <h5 class="card-title">'.$study->naam.'</h5>
                            </div>
							<div class="card-footer" style="border-bottom: 12px solid '.$_CATEGORY->kleur.'">
                                <small class="text-muted">'.$study->type.' <span class="text-danger">|</span> Niveau '.$study->niveau.' <span class="text-danger">|</span> '.$study->duur.' jaar</small>
                            </div>
                        </div>
                        </a>
                    </div>';
           
           }
           ?>

        </div>



<style>
        
</style>