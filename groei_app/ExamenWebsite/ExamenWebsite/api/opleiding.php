<?php
    /*
     * De headers voor de cors policy
     */
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET');
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    header('Content-Type: application/json');

    require "../inc/config.php";



    $id = $_GET["opleiding_id"] ?? "";

    /*
     * Alle gegevens uit de database halen.
     */
    $query = $db->prepare("
        SELECT opleiding.id, opleiding.naam, opleiding.duur, opleiding.niveau, type.naam type, opleiding.landschap_id, content.content, afbeelding.url img, landschap.kleur FROM opleiding
        INNER JOIN content ON opleiding.content_id = content.id
        INNER JOIN landschap ON opleiding.landschap_id = landschap.id
		INNER JOIN type ON opleiding.type_id = type.id
        LEFT JOIN afbeelding ON opleiding.afbeelding_id = afbeelding.id
        WHERE opleiding.id = :opleiding_id
    ");

    $query->bindValue(":opleiding_id", $id, PDO::PARAM_INT);

    /*
     * Query uitvoeren en het resultaat terug sturen als JSON
     */
    if ($query->execute()) {
        echo json_encode($query->fetch(PDO::FETCH_OBJ));
    } else echo "[]";


?>