
<div class="container">
    <div class="card-deck">
        <!--weergeven van de opgehaalde data -->
        <?php
        foreach($allCategories as $singleCategory){
            echo '<div class="col-sm-4"><a href="landschap.php?id=' . $singleCategory['id'] . '">
            <div class="card mb-4">
                <img class="card-img-top" src="uploads/' . $singleCategory['url'] . '" alt="Card image cap" />
                <div class="card-body" style="border-bottom: 12px solid ' . $singleCategory['color'] . ';">
                    <h5 class="card-title">' . $singleCategory['name'] . '</h5>
                </div>
            </div>
            </a>
          </div>';

        }
        ?>
    </div>
</div>