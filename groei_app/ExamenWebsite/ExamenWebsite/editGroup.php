<?php
require "inc/config.php";



// ophalen data

$group = $db->prepare("SELECT * FROM groups WHERE id=" . $_GET['id']);
$currentIndex = 0;
$groupsArray = array();

if ($group->execute()) {
    $allGroups = $group->fetchAll(PDO::FETCH_OBJ);
}

foreach($allGroups as $singleGroup){


    $groupsArray[$currentIndex]['name'] = $singleGroup->name;
    $groupsArray[$currentIndex]['id'] = $singleGroup->id;
    $groupsArray[$currentIndex]['content'] = $singleGroup->description;

    $currentIndex++;


}

if(isset($_POST['submit_group'])){
    // Voorbereiden insert querys opleiding en content
    $groupChangeName = $db->prepare("UPDATE `groups` SET name=:name WHERE id=" . $_GET['id']);
    $groupChangeContent = $db->prepare("UPDATE `groups` SET description=:content WHERE id=" . $_GET['id']);

    if($groupsArray[0]['name'] !== $_POST['name']){
        $groupChangeName->bindValue('name', $_POST['name']);
        $groupChangeName->execute();
    }

    if($groupsArray[0]['content'] !== $_POST['content']){
        $groupChangeContent->bindValue('content', $_POST['content']);
        $groupChangeContent->execute();
    }

	addMessage("Succesvol aangepast", 'De groep <b>'.$_POST['name'].'</b> is succesvol aangepast.', 'success');
	header("Location: " . $_SERVER['REQUEST_URI']);
	exit;
}



$_VIEW = "views/editGroup.php";

require_once "templates/default.php";