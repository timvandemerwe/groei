<div class="container">
    <div id="loginform">
        <h1 id="logintitel">
            Wachtwoord vergeten
        </h1>
        <!--gegevens kunnen veranderen-->
        <form method="post">
            <label class="loginlabel" for="username">Email adres</label>
            <br />
            <input class="logininput" type="email" name="email" id="email" placeholder="Email" />
            <br />
            <input name="submit" type="submit" id="submitlogin" value="Versturen" />
        </form>
        <br />
        <?php echo getError("login"); ?>
    </div>
</div>