<?php

    /*
     * De headers voor de cors policy
     */
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET');
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    header('Content-Type: application/json');
	
    require "../inc/config.php";
	
    /*
     * Alle gegevens uit de database halen.
     */
    $query = $db->prepare("
        SELECT landschap.id, landschap.naam, content.content, afbeelding.url img, landschap.kleur FROM landschap
        INNER JOIN content ON landschap.content_id = content.id
        LEFT JOIN afbeelding ON landschap.afbeelding_id = afbeelding.id;
    ");

    /*
     * Query uitvoeren en het resultaat terug sturen als JSON
     */
    if ($query->execute()) {
        echo json_encode($query->fetchAll(PDO::FETCH_OBJ));
    } else echo "[]";


?>