<?php

require "inc/config.php";

checkLogin();

$_USERS = array();

$query = $db->prepare("SELECT id, email, voornaam, achternaam FROM user");

if ($query->execute()) {
    $_USERS = $query->fetchAll(PDO::FETCH_OBJ);
}

$_VIEW = "views/users.php";

require_once "templates/default.php";