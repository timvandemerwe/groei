<?php

require "inc/config.php";

$_ALLSTUDIES = array();

/* LANDSCHAP OPHALEN */
// query voorberijden ophalen landschap met bepaald id
$category = $db->prepare("
        SELECT landschap.id, landschap.naam, content.content, afbeelding.url img, landschap.kleur FROM landschap
        INNER JOIN content ON landschap.content_id = content.id
        LEFT JOIN afbeelding ON landschap.afbeelding_id = afbeelding.id
        WHERE landschap.id = :id;
");

$category->bindValue(":id", $_GET['id'], PDO::PARAM_INT);

// haal het landschap op met dit id en zet het object in een var
if ($category->execute()) {
    $_CATEGORY = $category->fetch(PDO::FETCH_OBJ);
}


$studyQuery = $db->prepare("
        SELECT opleiding.id, opleiding.naam, opleiding.duur, opleiding.niveau, type.naam type, content.content, afbeelding.url img FROM opleiding
        INNER JOIN content ON opleiding.content_id = content.id
        INNER JOIN type ON opleiding.type_id = type.id
        LEFT JOIN afbeelding ON opleiding.afbeelding_id = afbeelding.id
        WHERE opleiding.landschap_id = :id;
");

$studyQuery->bindValue(":id", $_GET['id'], PDO::PARAM_INT);

if ($studyQuery->execute()) {
    $_ALLSTUDIES = $studyQuery->fetchALL(PDO::FETCH_OBJ);
}



/* EINDE OPLEIDING */

$_VIEW = "views/landschap.php";

require_once "templates/default.php";


