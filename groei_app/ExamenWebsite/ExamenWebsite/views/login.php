<div class="container">
    <div id="loginform">
        <h1 id="logintitel">
            Log in
        </h1>
        <!--inputs om je gegevens bij in te vullen om in te loggen-->
        <form method="post">
            <label class="loginlabel" for="username">E-mailadres</label>
            <br />
            <input class="logininput" type="email" name="email" id="email" placeholder="E-mailadres" />
            <br />

            <label class="loginlabel" for="password">Wachtwoord</label>
            <br />
            <input class="logininput" type="password" name="password" id="password" placeholder="Wachtwoord" />
            <br />

            <input name="submit" type="submit" id="submitlogin" value="Log in" />
        </form>
        <br />
        <?php echo getError("login"); ?>
    </div>
</div>