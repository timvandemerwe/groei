-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 05 dec 2019 om 12:09
-- Serverversie: 10.4.8-MariaDB
-- PHP-versie: 7.2.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `examen2019`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `afbeelding`
--

CREATE TABLE `afbeelding` (
  `id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `afbeelding`
--

INSERT INTO `afbeelding` (`id`, `url`) VALUES
(1, '68e8a93d959b31f2f17c615ee1479862.png'),
(2, 'c7dfe829ed1f799864869ad76387dfcf.png'),
(3, '72ee48a32c18da9e51d13a4c5f96ec35.png'),
(4, 'b4553578a539c45919dc47428d891a46.png'),
(5, '96b295caef2e27efd2dda24592adf24c.png'),
(6, '76869ad0de2adb20b36aca9fd2ccccd9.png'),
(7, 'c57ed4a97959d790e69dab3b6b6583c0.png'),
(8, 'a8b6bf9d08847297d4ae88d5764d38c8.png'),
(9, 'b1b155ac9973fa5773730ce28882abd3.png'),
(10, '8c98188090097afc90bac5ee167e98a4.png'),
(11, '0b4f14b9e9c47bca43b59d5ff4b01985.png'),
(12, 'bd4e6210aed4c4bfc3450875166ab100.png'),
(13, 'e796a12075acfe36b25b464f9b8e4093.png'),
(14, '55f72fac39461d98d36e5b4d6f944168.png'),
(15, '401f2e4bafa5e5c89f46591831fc3209.png'),
(16, 'e2501a30dcef4db851cb97debf469edb.png'),
(17, '34e88058d3466b88a00316fdb9334484.png'),
(18, 'ddca26eb69f19a608a0bfca98bf7768c.jpg'),
(19, 'f50650133df7da34fc497c0bb79b7c03.jpg'),
(20, '71bb350804fdde7bd53a469eeb1ed86e.jpg'),
(21, '1d1f4f2ab5597ba3b6e1d8a68a0de10d.jpg'),
(22, '10a4aec345b2c7f2f6aaaedb658f3283.png');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `content`
--

CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `content`
--

INSERT INTO `content` (`id`, `content`) VALUES
(1, 'dit is een zin.'),
(2, 'dit is een zin.'),
(3, 'dit is een zin.'),
(4, 'dit is een zin.'),
(5, 'dit is een zin.'),
(6, 'dit is een zin.'),
(7, 'hier komt iets'),
(8, 'HET is een schande'),
(9, 'test'),
(10, 'test'),
(11, 'test'),
(12, 'jo jo dit is een beschrijving jo'),
(13, 'test'),
(14, 'test'),
(15, 'test'),
(16, 'test'),
(17, 'test'),
(18, 'test'),
(19, 'test'),
(20, 'test'),
(21, 'test'),
(22, 'test'),
(23, 'test'),
(24, 'test'),
(25, 'test'),
(26, 'test'),
(27, 'test'),
(28, 'test'),
(29, 'test'),
(30, 'leuk'),
(31, 'poep'),
(32, 'poep'),
(33, 'poep'),
(34, 'poep'),
(35, 'poep'),
(36, 'test'),
(37, 'test'),
(38, 'test'),
(39, 'test'),
(40, 'test'),
(41, 'test'),
(42, 'test'),
(43, 'Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren \'60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals Aldus PageMaker die versies van Lorem Ipsum bevatten.'),
(44, 'test'),
(45, 'test'),
(46, 'kleur'),
(47, 'kleur'),
(48, ''),
(49, 'dit is een zi'),
(50, 'dit is een zin.'),
(51, 'dit is een zinetje');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `landschap`
--

CREATE TABLE `landschap` (
  `id` int(11) NOT NULL,
  `naam` varchar(255) NOT NULL,
  `content_id` int(11) NOT NULL,
  `afbeelding_id` int(11) DEFAULT NULL,
  `kleur` varchar(30) NOT NULL DEFAULT '#ED135D'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `landschap`
--

INSERT INTO `landschap` (`id`, `naam`, `content_id`, `afbeelding_id`, `kleur`) VALUES
(1, 'ICT en Technologie', 50, 1, ''),
(2, 'test', 13, 5, ''),
(3, 'test', 14, 6, ''),
(4, 'test', 25, NULL, ''),
(5, 'test zonder image', 28, 10, ''),
(6, 'test met image', 29, 11, ''),
(7, 'ICT', 30, 12, ''),
(8, 'Test Landschap', 43, 18, ''),
(9, 'test', 44, 19, ''),
(10, 'kleur', 46, 21, ''),
(11, 'kleur', 47, NULL, '#42ed13'),
(12, '', 48, NULL, '');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `opleiding`
--

CREATE TABLE `opleiding` (
  `id` int(11) NOT NULL,
  `naam` varchar(255) NOT NULL,
  `duur` int(1) NOT NULL,
  `niveau` int(1) NOT NULL,
  `type_id` int(11) NOT NULL,
  `landschap_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `afbeelding_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `opleiding`
--

INSERT INTO `opleiding` (`id`, `naam`, `duur`, `niveau`, `type_id`, `landschap_id`, `content_id`, `afbeelding_id`) VALUES
(15, 'test', 4, 3, 2, 7, 51, 22),
(16, 'test', 3, 4, 1, 1, 1, NULL),
(17, 'test', 3, 4, 1, 1, 1, NULL),
(18, 'test', 3, 3, 1, 1, 1, NULL),
(19, 'test', 3, 3, 1, 1, 1, NULL),
(20, 'test', 3, 3, 1, 1, 1, NULL),
(21, 'test', 3, 3, 1, 1, 2, NULL),
(22, 'test', 3, 3, 1, 1, 3, NULL),
(23, 'test', 3, 3, 1, 1, 4, NULL),
(24, 'V', 3, 3, 1, 1, 5, NULL),
(25, 'test2', 3, 3, 1, 1, 6, NULL),
(26, '1sadf', 3, 4, 1, 1, 7, NULL),
(27, 'Boereh', 5, 4, 2, 1, 8, NULL),
(28, 'afbeelding', 1, 1, 1, 1, 9, NULL),
(29, 'afbeelding', 1, 1, 1, 1, 10, 2),
(30, 'afbeelding', 1, 1, 1, 1, 11, 3),
(31, 'JE BOY', 3, 4, 2, 1, 12, 4),
(32, 'test na crash', 1, 1, 1, 1, 16, 7),
(33, 'test na crash', 1, 1, 1, 1, 17, 8),
(34, 'test', 1, 1, 1, 1, 20, NULL),
(35, 'test', 1, 1, 1, 1, 21, NULL),
(36, 'test zonder image', 1, 1, 1, 1, 26, NULL),
(37, 'test met image', 1, 1, 1, 9, 27, 9),
(38, 'test na crash', 4, 3, 3, 9, 31, 13),
(39, 'test na crash', 4, 3, 3, 9, 32, 14),
(40, 'test na crash', 4, 3, 3, 6, 33, 15),
(41, 'test na crash', 4, 3, 3, 6, 34, 16),
(42, 'test na crash', 4, 3, 3, 6, 35, 17),
(43, 'test na crash', 1, 1, 1, 1, 36, NULL),
(44, 'test na crash', 1, 1, 1, 1, 37, NULL),
(45, 'test na crash', 1, 1, 1, 1, 38, NULL),
(46, 'test na crash', 1, 1, 1, 1, 39, NULL),
(47, 'test na crash', 1, 1, 1, 1, 40, NULL),
(48, 'test', 1, 1, 1, 1, 41, NULL),
(49, 'test', 1, 1, 1, 1, 42, NULL),
(50, 'Opleiding', 1, 1, 1, 9, 45, 20);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `naam` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `type`
--

INSERT INTO `type` (`id`, `naam`) VALUES
(1, 'BOL'),
(2, 'BBL'),
(3, 'BOL / BBL');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `wachtwoord` varchar(255) NOT NULL,
  `voornaam` varchar(255) NOT NULL,
  `achternaam` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `user`
--

INSERT INTO `user` (`id`, `email`, `wachtwoord`, `voornaam`, `achternaam`) VALUES
(1, 'test@test.nl', '$2y$10$mAilhP4knq2cZ1dUBh3EleCY7yVyWqqG8C7988blCoiCmnX8udc62', 'test', 'test');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `afbeelding`
--
ALTER TABLE `afbeelding`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `landschap`
--
ALTER TABLE `landschap`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `opleiding`
--
ALTER TABLE `opleiding`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `afbeelding`
--
ALTER TABLE `afbeelding`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT voor een tabel `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT voor een tabel `landschap`
--
ALTER TABLE `landschap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT voor een tabel `opleiding`
--
ALTER TABLE `opleiding`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT voor een tabel `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT voor een tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
