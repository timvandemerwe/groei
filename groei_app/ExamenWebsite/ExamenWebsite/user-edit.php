<?php

require "inc/config.php";

checkLogin();

$id = $_GET["id"] ?? "";

/*
 * Kijken of het formulier is verzonden. Daarna deze gebruiker bewerken.
 */
if (isset($_POST["submit"])) {
    $email = $_POST["email"];
    $voornaam = $_POST["voornaam"];
    $achternaam = $_POST["achternaam"];
    $wachtwoord = $_POST["wachtwoord"];

    $query = $db -> prepare("UPDATE user SET email = :email, voornaam = :voornaam, achternaam = :achternaam WHERE id = :id");
    $query->bindValue(":email", $email, PDO::PARAM_STR);
    $query->bindValue(":voornaam", $voornaam, PDO::PARAM_STR);
    $query->bindValue(":achternaam", $achternaam, PDO::PARAM_STR);

    $query->bindValue(":id", $id, PDO::PARAM_INT);

    if ($query -> execute()) {
        
    } else {
        
    }
}


/*
 * De gebruiker voor het weergeven van de data ophalen.
 */
$query = $db->prepare("SELECT id, email, voornaam, achternaam FROM user WHERE id = :id");
$query-> bindValue(':id', $id, PDO::PARAM_INT);

if ($query->execute()) {
    $_USER = $query->fetch(PDO::FETCH_OBJ);
}

if (!$_USER) {
    header("Location: users.php");
}

$_VIEW = "views/user-edit.php";

require_once "templates/default.php";