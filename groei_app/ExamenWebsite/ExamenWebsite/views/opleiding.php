<div id="header-img" class="" style="background-image: url('<?php echo (isset($allStudies[0]['url']) ? "uploads/".$allStudies[0]['url'] : "assets/img/landstedeheader.jpg")?>')">
  <?php //echo '<img src="' .  ($_CATEGORY->img ? "uploads/".$_CATEGORY->img : "assets/img/landstedeheader.jpg") . '"  id="" class="" alt="..." />'?>
</div>

<!-- opleiding tekst-->
<div class="container">
    <div class="container">
        <div class="tekstdesign">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="landschap.php?id=<?php echo $allStudies[0]['landschap_id']; ?>"><?php echo $allStudies[0]['landschap'];?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo $allStudies[0]['name']; ?></li>
                </ol>
            </nav>

            <!-- opleiding naam met content-->
            <h1>
                <?php echo $allStudies[0]['name']; ?>
            </h1>
            <div>
                <?php echo $allStudies[0]['content']; ?>
            </div>
            <br />
            <hr />
            <br/>
            <!-- opleiding informatie-->
            <h6>Opleiding informatie:</h6>
            <div>
                <?php echo $allStudies[0]['time'] . ' jaar';?>
            </div>
            <div>
                <?php echo $allStudies[0]['type']; ?>
            </div>
            <div>
                <?php echo 'Niveau ' . $allStudies[0]['niveau']; ?>
            </div>
        </div>

        <!--knop return to landschap
        <a class="btn btn-primary terugknop" href="opleiding.php?id=" id="inputopleiding" role="button">Terug naar opleiding</a>-->
    </div>
</div>