<div class="container">
    <div id="registerform">
        <h1 id="registertitel">
            Gebruiker bewerken
        </h1>
        <!--form om gebruiker gegevens te kunnen aanpassen-->
        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
            <label class="registerlabel" for="email">Email adres</label>
            <br />
            <input class="registerinput" name="email" type="text" placeholder="Email" value="<?php echo $_USER->email; ?>" />
            <br />

            <label class="registerlabel" for="wachtwoord">Wachtwoord</label>
            <br />
            <input class="registerinput" name="wachtwoord" type="password" placeholder="Wachtwoord" />
            <br />

            <label class="registerlabel" for="voornaam">Voornaam</label>
            <br />
            <input class="registerinput" name="voornaam" type="text" placeholder="Voornaam" value="<?php echo $_USER->voornaam; ?>" />
            <br />

            <label class="registerlabel" for="achternaam">Achternaam</label>
            <br />
            <input class="registerinput" name="achternaam" type="text" placeholder="Achternaam" value="<?php echo $_USER->achternaam; ?>" />
            <br />

            <input id="registersubmit" name="submit" type="submit" value="Registreer" />
        </form>
        
    </div>
</div>