<?php

/*
 * Connectie maken met de database!
 */
function connectDB(){
    global $db;

    try {
        $db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
        $db->exec('SET NAMES utf8');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    }
    catch( PDOException $ex ) {
        die( $ex->getMessage() );

    }
}

/*
 *
 * Kijken of de gebruiker is ingelogd.
 *
 */
function checkLogin() {
    if (!isLoggedIn()) {
		header("Location: login.php");
        exit;
	}
}

/*
 *
 * Kijken of de gebruiker is ingelogd via een sessie.
 *
 */
function isLoggedIn() {
	return isset($_SESSION["UID"]);
}

/*
 *
 * Een bericht voor een popup toe te voegen.
 *
 */
function addMessage($title, $content, $icon = "info") {
	global $_NEWMESSAGES;

	$_NEWMESSAGES[] = array("title" => $title, "text" => $content, "icon" => $icon);

	$_SESSION["messages"] = json_encode($_NEWMESSAGES);
}

/*
 *
 * Kijken of de gebruiker is ingelogd en in de database bestaat.
 *
 */
function checkPreLogin() {
	global $db;
	if (isLoggedIn()) {
		$query = $db->prepare("SELECT id FROM user WHERE id = :id LIMIT 1");
		$query -> bindValue(":id", $_SESSION["UID"], PDO::PARAM_INT);

		if ($query -> execute()) {
			if ($res = $query->fetch()) {
				return true;
			}

			unset($_SESSION["UID"]);

			checkLogin();
		}
	}

	return false;
}

/*
 *
 * Een error ophalen uit de sessie.
 *
 */
function getError($id){
    global $_VIEWDATA;
    if (isset($_VIEWDATA[$id])) {
        return '<span class="text-danger">'.$_VIEWDATA[$id].'</span>';
    } else {
        return "";
    }
}

function getUrl($url) {
    return $url . ".php";
}