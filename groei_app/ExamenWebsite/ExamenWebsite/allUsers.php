<?php
require "inc/config.php";

// Check of de gebruiker is ingelogd stuur anders door naar login
if(!isset($_SESSION['UID'])){
    header("Location:Login.php");
}
    // query voor het ophalen van alle landschappen
    $groups = $db->prepare("SELECT * FROM groups");
    $currentIndex = 0;
    $allGroups = array();

    // haal alle landschappen op
    if ($groups->execute()) {
        $_ALLGROUPS = $groups->fetchAll(PDO::FETCH_OBJ);
    }


    // loop door alle landschapen heen
    foreach($_ALLGROUPS as $singleGroup){



        // vul de rest van het landschap aan met de overige data
        $allGroups[$currentIndex]['name'] = $singleGroup->name;
        $allGroups[$currentIndex]['id'] = $singleGroup->id;
        $allGroups[$currentIndex]['content'] = $singleGroup->description;



        // maak een query om alle opleidingen binnen dit landschap op te halen
        $user = $db->prepare("SELECT * FROM users WHERE group_id=" . $singleGroup->id);

        // haal de opleidingen binnen het landschap op
        if ($user->execute()) {
            $allusers = $user->fetchAll(PDO::FETCH_OBJ);
        }

        // loop door alle opleidingen heen
        foreach($allusers as $singleUser){
            $cur = array();

            // voeg de rest van de data in bij de opleiding
            $cur['name'] = $singleUser->email;
            $cur['id'] = $singleUser->id;

            // sla de opleiding op in een array
            $allGroups[$currentIndex]['users'][] = $cur;


        }
        $currentIndex++;
    }




    $_VIEW = "views/allUsers.php";

require_once "templates/default.php";

