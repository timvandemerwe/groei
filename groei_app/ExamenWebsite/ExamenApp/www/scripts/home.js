﻿


var app = {
    // Application Constructor
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function () {
        console.log("Ready");

        $("#scan").on('click', function (e) {
            e.preventDefault();
            console.log("scan");
            window.QRScanner.prepare(onDone); 
            console.log("scan1");
        });

        $("#exit_button").on('click', function (e) {
            e.preventDefault();
            hide(400);
        });


    }
};

function onDone(err, status) {
    if (err) {
        // here we can handle errors and clean up any loose ends.
        console.error(err);
    }
    if (status.authorized) {
        // W00t, you have camera access and the scanner is initialized.
        // QRscanner.show() should feel very fast.

        show(400);
        console.log("show");
    } else if (status.denied) {
        // The video preview will remain black, and scanning is disabled. We can
        // try to ask the user to change their mind, but we'll have to send them
        // to their device settings with `QRScanner.openSettings()`.
    } else {
        // we didn't get permission, but we didn't get permanently denied. (On
        // Android, a denial isn't permanent unless the user checks the "Don't
        // ask again" box.) We can ask again at the next relevant opportunity.
    }
}

function displayContents(err, text) {
    if (err) {
        // an error occurred, or the scan was canceled (error code `6`)
    } else {
        // The scan completed, display the contents of the QR code:
        alert(text);
    }
}

function show(time) {
    window.QRScanner.show();
    setTimeout(function () {
        QRScanner.scan(displayContents);
    }, time + 100);
    $(".app").fadeOut(time);
    $("#scan-view").fadeIn(time);
    $("body").animate({
        backgroundColor: "rgba(255, 255, 255, 0)"
    }, time);
}


function hide(time) {
    
    setTimeout(function () {
        QRScanner.hide();
    }, time + 100);
    $(".app").fadeIn(time);
    $("#scan-view").fadeOut(time);
    $("body").animate({
        backgroundColor: "rgba(255, 255, 255, 1)"
    }, time);
}

app.initialize();

    // Make the webview transparent so the video preview is visible behind it.
    
    // Be sure to make any opaque HTML elements transparent here to avoid
    // covering the video.

