<!DOCTYPE html>
<html lang=nl>
<head>
    <meta charset=utf-8 />
    <!--css toevoegen-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/main.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/blok.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/backend.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/backend.css" />

    <link href="assets/css/fontawesome.css" rel="stylesheet" />
    <link href="assets/css/brands.css" rel="stylesheet" />
    <link href="assets/css/solid.css" rel="stylesheet" />

    <link rel="stylesheet" href="assets/farbtastic/farbtastic.css" type="text/css" />
    <link rel="stylesheet" href="assets/plugins/toast/jquery.toast.min.css" type="text/css" />
    <title>Landstede QR-scanner</title>
</head>
<!--standaard template-->
<body>
    <div class="col-md-3 col-sm-4 logo">
        <a class="mbo-navigation-logo" href="/">
            <img src="assets/img/groei_logo.png" />
        </a>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light fixed-top">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">
                        Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="landschappen.php">
                        Landschappen
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <!--komt te voorschijn in het menu als je bent ingelogd-->
                <?php
                if(isLoggedIn())
                {
                    echo '<li><a class="nav-link all" href="allUsers.php">Groei-leden Beheren</a></li>';
                    echo '<li><a class="nav-link all" href="users.php">Gebruikers Beheren</a></li>';
                    echo '<li><a class="nav-link logout" href="logout.php">Uitloggen</a></li>';
                } else {
                    echo '<li><a class="nav-link login" href="login.php">Inloggen</a></li>';
                }

                ?>
            </ul>
        </div>
    </nav>


    <?php
    /*
     * Kijken of de view bestaat en dan de view inladen.
     */
    if (isset($_VIEW) && file_exists($_VIEW)) {
        require_once $_VIEW;
    }
    ?>
    <!--script toevoegen-->
    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="assets/farbtastic/farbtastic.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/bootbox.min.js"></script>
    <script src="assets/js/CMS.js"></script>

    <script src="assets/plugins/tinymce/tinymce.min.js"></script>
    <script src="assets/plugins/toast/jquery.toast.min.js"></script>


    <script src="assets/js/main.js"></script>

    <script type="text/javascript">
		var messages = <?php echo json_encode($_MESSAGES); ?>;

		for (var i = 0; i < messages.length; i++) {
			var temp = messages[i];

            // Een nieuwe toast aanmaken met de php lijst van de sessie
			$.toast({
				heading: temp.title,
				text: temp.text,
				icon: temp.icon,
				position: 'top-right',
				showHideTransition: 'fade',
				loader: false,
				stack: 10
			})
		}


    </script>
</body>
</html>



