﻿$(function() {
    tinymce.init({
        selector: '.custom-textarea'
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-confirm-box="delete"]').on('click', function(e) {
        e.preventDefault();
        var element = $(this);

        var action = element.data('action-url') ? element.data('action-url') : "";
        var msg = element.data('message') ? element.data('message') : "Weet u het zeker?";

        bootbox.confirm({
            size: "large",
            message: msg,
            backdrop: true,
            buttons: {
                confirm: {
                    label: 'Ja',
                    className: 'btn-danger'
                },
                cancel: {
                    label: 'Nee',
                    className: 'btn-secondary'
                }
            },
            callback: function(result) {
                if (result) {
                    window.location.href = action;
                }
            }

        });
    });

    try {
        $('#colorpicker').farbtastic('#color');
    } catch (error) {

    }
});

function randomPassword() {

    document.getElementById("password").value = Math.random().toString(36).slice(-8);;

}