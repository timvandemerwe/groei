<?php
    require "inc/config.php";


	//Kijken of de gebruiker is ingelogd zo ja log hem uit.
    if (isset($_SESSION["UID"])) {
        $_SESSION["UID"] = null;
        header('location: index.php');
        exit;
    }
