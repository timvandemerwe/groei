<?php
require "inc/config.php";

$category = $db->prepare("SELECT * FROM landschap");
$currentIndex = 0;
$allCategories = array();

// Alle categorieen ophalen.
if ($category->execute()) {
    $_ALLCATEGORIES = $category->fetchAll(PDO::FETCH_OBJ);
}

foreach($_ALLCATEGORIES as $singleCategory){
    $content = $db->prepare("SELECT content FROM content WHERE id=" . $singleCategory->content_id);
    if ($content->execute()) {
        $singleContent = $content->fetchAll(PDO::FETCH_OBJ);
    }

    if(isset($singleCategory->afbeelding_id)){
        $image = $db->prepare("SELECT url FROM afbeelding WHERE id=" . $singleCategory->afbeelding_id);
        if ($image->execute()) {
            $singleImage = $image->fetchAll(PDO::FETCH_OBJ);
        }
        $allCategories[$currentIndex]['url'] = $singleImage[0]->url;

    }

    $allCategories[$currentIndex]['name'] = $singleCategory->naam;
    $allCategories[$currentIndex]['id'] = $singleCategory->id;
    $allCategories[$currentIndex]['color'] = $singleCategory->kleur;
    $allCategories[$currentIndex]['content'] = $singleContent[0]->content;

    $currentIndex++;


}



$_VIEW = "views/home.php";

require_once "templates/default.php";

