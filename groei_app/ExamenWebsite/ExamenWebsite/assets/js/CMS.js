﻿$('#typeOfPage').on('change', function() {
    if (this.value == 1) {
        $('#studyForm').toggleClass('hide');
        $('#categoryForm').addClass('hide');
} else if(this.value == 2) {
        $('#categoryForm').toggleClass('hide');
        $('#studyForm').addClass('hide');
}
});

/* Als het verwijderen wordt bevestigd werkt de functie van de knop */
$('#deleteModal').on('show.bs.modal', function (e) {
    $(this).find('.confirm-delete').attr('href', $(e.relatedTarget).data('href'));
});
