<?php

/**
 * User short summary.
 *
 * User description.
 *
 */
class User
{
    private $db;
    public $id, $email, $wachtwoord, $voornaam, $achternaam;

    public function __construct($db, $email, $wachtwoord)
    {
        $this->db = $db;
        $this->email = $email;
        $this->wachtwoord = $wachtwoord;
    }

    public function exists() {
        $query = $this->db->prepare("SELECT id FROM user WHERE LOWER(email) = LOWER(:email)");
        $query->bindValue(':email', $this->email, PDO::PARAM_STR);

        if ($query->execute()) {
            if ($query->rowCount() > 0) {
                return true;
            }
        }

        return false;
    }

    public function login() {
        $query = $this->db->prepare("SELECT id, wachtwoord FROM user WHERE LOWER(email) = LOWER(:email)");
        $query->bindValue(':email', $this->email, PDO::PARAM_STR);

        if ($query->execute()) {
            if ($res = $query->fetch(PDO::FETCH_OBJ)) {
                $this->id = $res->id;
                return $this->passwordVerify($res->wachtwoord);
            }
        }

        return false;
    }

    public function register() {
        $query = $this->db->prepare("
            INSERT INTO user (email, wachtwoord, voornaam, achternaam)
            VALUES (:email, :wachtwoord, :voornaam, :achternaam)
         ");

        $query->bindValue(':email', $this->email, PDO::PARAM_STR);
        $query->bindValue(':wachtwoord', $this->password(), PDO::PARAM_STR);
        $query->bindValue(':voornaam', $this->voornaam, PDO::PARAM_STR);
        $query->bindValue(':achternaam', $this->achternaam, PDO::PARAM_STR);

        if ($query->execute()) {
            return true;
        }

        return false;
    }

    private function salt()
    {
        return "EX".$this->wachtwoord."dFghJYtfGtFDFBTN";
    }

    private function password()
    {
        return password_hash($this->salt(), PASSWORD_DEFAULT);
    }

    private function passwordVerify($hash)
    {
        return password_verify($this->salt(), $hash);
    }
}